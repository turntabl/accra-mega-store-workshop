##  💲💲💲 Welcome to the Accra Mega Store 💲💲💲

**Accra Mega Store Ltd** is a client of the consulting organisation you work for and have asked your company to build them an online store where their customers can purchase items.

The main developer in charge of the project recently left the company and you’ve now been tasked with completing the 
project. Fortunately  the developer completed most of the features before they left however, they’ve not done a 
great job at writing tests! They’ve also not ticked off which of the features they have completed so you’ll have to 
figure it out all on your own by reading the code. 😬

The client is bent on having 100% feature and at least 80% test coverage and will not pay for the application unless 
you can tick off all features and verify that you’ve tested the application and all feature works!

## User requirements
1. **All available products should be uploaded to the online store  🌟**
   1. Each week, the warehouse manager receives a delivery of new products. The warehouse manager 
      provides the software development team with a list of all the products in the warehouse both new and old. This 
      list includes the product id, description, category, price and quantity. This csv file is to be uploaded and 
      used by the online store. 
   2. A product id is unique and no two different products will have the same id. Sometimes however, the warehouse 
      manager adds the same product twice so you might find the same duplicate record in the csv. if this happens 
      ignore one because all the values will be identical.
   3. Sometimes, when a new product is received, the quantity or price might be missing. this might be because the 
      stocking team is yet to fully complete the order from their own wholesaler. if this happens, ignore the 
      product because it's not sellable.
   4. When a user is shopping using the application, the user should be able to get a list of all the products
      stocked by the store. This list should include all products even if the quantity available is zero.

2. **A user should be able to have a full shopping experience 🌟**
   1. A user should be able to interact with the online store via the command line. This is the MVP (Minimum Viable 
      Product), however, in the future , it will be upgraded into a sophisticated web application with a fancy ui.
   2. A user should be able to add items to their shopping cart. Its important that they can only add items that are 
      in stock! When they add an item, the available products stock level should be adjusted accordingly. If a 
      user tries to add an item that is Out of stock, let them know its out of stock and try to make a recommendation 
      of a similar product. A product is considered similar to another if they have the same name and category.
   3. When a user adds an item to the basket, make sure to suggest complementary products. e.g. if someone buys milk,
      they'll probably want sugar and tea. Luckily for you, you don't have to write any complex algorithms, the warehouse manager has provided a list of products 
      that complement each other. You just need to remember to suggest these products if they are in stock.
   4. A user should be able to remove items from their basket. When a user does this, the product stock level should 
      be adjusted accordingly.
   5. A user should be able to checkout the items they have in their basket and taxes and discounts should be 
      calculated. A user should receive a 3 for 2 offer if they order more than 3 of the same item. Tax should be a 
      flat rate of 0.5% for the first 100 GHC and then 1% tax thereafter. 
   6. Once the user checks out, the updated products will be sent to the warehouse manager and the file will be updated so that next time a user visits the store, the correct level of 
      items is displayed. When a user checkouts out, they will receive an order item and the order will be persisted 
      in a file that will manually be sent to the warehouse manager every hour so that it can be shipped to the user.

Good Luck!! 🚀🚀🚀
